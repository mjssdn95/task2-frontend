import React, { useState, useEffect } from "react";
import { Button, Input, message, List, Pagination } from "antd";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import "./antd/dist/antd.css";
import "./App.css";

const App = () => {
  const [task, setTask] = useState("");
  const [date, setDate] = useState(null);
  const [reminders, setReminders] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    const storedReminders = JSON.parse(localStorage.getItem("reminders"));
    if (storedReminders && storedReminders.length > 0) {
      setReminders(storedReminders);
    }
  }, []);

  const handleAddReminder = () => {
    if (!task || !date) {
      message.error("Vui lòng điền đủ thông tin");
      return;
    }
    const newReminder = { task, date };
    setReminders([...reminders, newReminder]);
    setTask("");
    setDate(null);
    localStorage.setItem(
      "reminders",
      JSON.stringify([...reminders, newReminder])
    );
    message.success("Đã thêm nhắc nhở mới");
  };

  const handleDeleteReminder = (index) => {
    const updatedReminders = [...reminders];
    updatedReminders.splice(index, 1);
    setReminders(updatedReminders);
    localStorage.setItem("reminders", JSON.stringify(updatedReminders));
    message.success("Đã xóa nhắc nhở");
  };
  const onChangePage = (page) => {
    setCurrentPage(page);
  };

  const pageSize = 5;
  const indexOfLastReminder = currentPage * pageSize;
  const indexOfFirstReminder = indexOfLastReminder - pageSize;
  const currentReminders = reminders.slice(
    indexOfFirstReminder,
    indexOfLastReminder
  );

  return (
    <div className="App">
      <div className="input-container">
        <h2>App Nhắc Nhở</h2>
        <Input
          placeholder="Nhập task..."
          value={task}
          onChange={(e) => setTask(e.target.value)}
        />
        <DatePicker
          selected={date}
          onChange={(date) => setDate(date)}
          disabledKeyboardNavigation
          minDate={new Date()}
          dateFormat="dd/MM/yyyy"
        />

        <Button type="primary" onClick={handleAddReminder}>
          Thêm Nhắc Nhở
        </Button>
      </div>
      <div className="reminder-list">
        <h2>Danh sách nhắc nhở</h2>
        <List
          dataSource={currentReminders}
          renderItem={(item, index) => (
            <List.Item
              className={
                moment(item.date).isSame(moment(), "day") ? "highlighted" : ""
              }
              // Nếu ngày nhắc nhở bằng với ngày hiện tại, thì sử dụng class "highlighted"
              actions={[
                <Button
                  type="text"
                  danger
                  onClick={() =>
                    handleDeleteReminder(indexOfFirstReminder + index)
                  }
                  style={{ color: "red" }}
                >
                  Xóa
                </Button>,
              ]}
            >
              <div>
                <span>{item.task}</span> -{" "}
                <span style={{ margin: "0 auto" }}>
                  {moment(item.date).format("DD/MM/YYYY")}
                </span>
              </div>
            </List.Item>
          )}
        />

        <Pagination
          defaultCurrent={1}
          pageSize={pageSize}
          total={reminders.length}
          onChange={onChangePage}
          style={{ marginTop: "1rem", textAlign: "center" }}
        />
      </div>
    </div>
  );
};

export default App;
